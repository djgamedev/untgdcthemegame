extends Node

#Variables
onready var player = $SpacePlayer
onready var enemySpawnTimer = $EnemySpawnTimer
onready var enemySpawner = $EnemyPath/EnemySpawner
onready var scoreText = $ScoreText

export(PackedScene) var enemy

var score
var restartTimer = 5
var scoreTimer = 7.5

func _ready():
	enemySpawnTimer.start()
	scoreText.text = "Score: 0"
	score = 0
	pass

func _process(delta):
	scoreTimer -= delta
	scoreText.text = "Score: " + str(score)
	
	if get_node("SpacePlayer") == null:
		restartTimer -= delta
		scoreText.text = "GAME OVER! Final Score: " + str(score)
		if restartTimer <= 0:
			get_tree().reload_current_scene()
	
	if scoreTimer <= 0 and get_node("SpacePlayer") != null:
		score += 2
		scoreTimer = 7.5

func _on_EnemySpawnTimer_timeout():
	enemySpawner.set_offset(randi())
	
	var enemyInstance = enemy.instance()
	add_child(enemyInstance)
	
	var direction = enemySpawner.rotation + PI / 2
	
	enemyInstance.position = enemySpawner.position
	
	direction += rand_range(-PI / 4, PI / 4)
	enemyInstance.rotation = direction
	
	enemyInstance.set_linear_velocity(Vector2(rand_range(enemyInstance.minSpeed, enemyInstance.maxSpeed), 0).rotated(direction))
	
	enemySpawnTimer.wait_time = rand_range(.25, 1.75)
