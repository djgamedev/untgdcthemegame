extends Area2D

#Variables
onready var bulletLifeTimer = $BulletLifeTimer
onready var collision = $CollisionShape2D

var bulletSpeed = 750
var damage = 10
var velocity = Vector2()

func _ready():
	connect("body_entered", self, "OnBodyEntered")
	bulletLifeTimer.start()
	pass

func Start(_position, _direction):
	position = _position
	rotation = _direction.angle()
	velocity = _direction * bulletSpeed

func _process(delta):
	position += -velocity * delta
	

func OnBodyEntered(body):
	if body.is_in_group("Enemies"):
		body.Die()
		get_parent().score += 5
		queue_free()

func _on_BulletLifeTimer_timeout():
	queue_free()
