extends Area2D

# Variables
export var moveSpeed = 250
export var rotateSpeed = 2.5
export(PackedScene) var bullet

onready var bulletStartPos = $BulletSpawn
onready var sprite = $Sprite

var velocity = Vector2()
var rotationDir = 0
var screenWidth
var screenHeight
var spriteHeight
var spriteWidth
var isDead

func _ready():
	connect("body_entered", self, "OnBodyEntered")
	screenWidth = get_viewport_rect().size.x
	screenHeight = get_viewport_rect().size.y
	spriteHeight = sprite.texture.get_height()
	spriteWidth = sprite.texture.get_width()
	isDead = false
	pass

func _physics_process(delta):
	GetInput(delta)
	rotation += rotationDir * rotateSpeed * delta
	
	position.x = clamp(position.x, 0 + (spriteWidth / 2), screenWidth - (spriteWidth / 2))
	position.y = clamp(position.y, 0 + (spriteHeight / 2), screenHeight - (spriteHeight / 2))

func GetInput(delta):
	rotationDir = 0
	velocity = Vector2()
	if Input.is_action_pressed("Right"):
		rotationDir = 1
	if Input.is_action_pressed("Left"):
		rotationDir = -1
	if Input.is_action_pressed("Forward"):
		velocity = Vector2(0, -moveSpeed).rotated(rotation)
	if Input.is_action_pressed("Backward"):
		velocity = Vector2(0, moveSpeed).rotated(rotation)
	
	if Input.is_action_just_pressed("Shoot"):
		var dir = Vector2(0, 1).rotated(bulletStartPos.global_rotation)
		var bulletInstance = bullet.instance()
		get_parent().add_child(bulletInstance)
		bulletInstance.Start(position, dir)
		bulletInstance.position = position
		bulletInstance.rotation = rotation
	
	position += velocity * delta

func Die():
	isDead = true

func OnBodyEntered(body):
	if body.is_in_group("Enemies"):
		body.Die()
		queue_free()