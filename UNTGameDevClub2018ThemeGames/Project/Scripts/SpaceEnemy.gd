extends RigidBody2D

# Variables
export(float) var minSpeed
export(float) var maxSpeed

var scoreValue = 5

func _ready():
	connect("body_entered", self, "OnBodyEntered")
	pass

func Die():
	queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
